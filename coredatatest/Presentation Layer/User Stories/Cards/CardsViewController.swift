//
//  CardsViewController.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 07.09.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import CoreData

class CardsViewController: UITableViewController {
    lazy var dataProvider: CardsProtocol = {
        return CardsProvider(fetchedResultsController: self)
    }()
    
    private let tableManager = CardsTableManager()

    private var searchController: UISearchController {
        let controller = UISearchController(searchResultsController: nil)

        controller.searchBar.placeholder = "Поиск по картотеке"

        controller.dimsBackgroundDuringPresentation = false
        controller.hidesNavigationBarDuringPresentation = false

        return controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        configureNavigationBarMode()
        configureTableView()

        fetch()
    }
}

private extension CardsViewController {
    func configureTableView() {
        tableManager.configureTableView(tableView)
        tableManager.fetchedResults = dataProvider.fetchedResults
        
        tableManager.didDeleteCell = { [weak self] (indexPath) in
            self?.dataProvider.didDeleteObject(indexPath: indexPath)
        }

        tableManager.didTapEditNoteButton = { [weak self] (indexPath) in
            self?.didTapEditNoteButton(indexPath: indexPath)
        }

        tableManager.editModeSwipeOn = { [weak self] in
            self?.editModeSwipeOn()
        }

        tableManager.editModeSwipeOff = { [weak self] in
            self?.editModeSwipeOff()
        }

        toggleTableViewBackgroundView()
    }
}

private extension CardsViewController {
    func configureNavigationBar() {
        title = "Картотека"

        navigationController?.navigationBar.prefersLargeTitles = true
    }
}

private extension CardsViewController {
    func fetch() {
        dataProvider.fetch { [weak self] in
            self?.reloadView()
        }
    }

    func reloadView() {
        tableManager.fetchedResults = dataProvider.fetchedResults
        toggleTableViewBackgroundView()
    }
}

@objc private extension CardsViewController {
    func didTapAddNoteButton() {
        configureAlertTextField(alertTitle: CardStaticDataSource.new.title, alertMessage: CardStaticDataSource.new.subtitle)
    }

    func didTapEditNoteButton(indexPath: IndexPath) {
        configureAlertTextField(alertTitle: CardStaticDataSource.edit.title, alertMessage: CardStaticDataSource.edit.subtitle, indexPath: indexPath)
    }
}

private extension CardsViewController {
    func toggleTableViewBackgroundView() {
        guard let fetchedObjects = dataProvider.fetchedResults?.fetchedObjects else {
            tableView.backgroundView = presentEmptyView()

            return
        }

        tableView.backgroundView = fetchedObjects.isEmpty ? presentEmptyView() : nil
    }

    func presentEmptyView() -> EmptySubtitleCenterView {
        let emptyView = EmptySubtitleCenterView()
        emptyView.titleLabel.text = CardStaticDataSource.emptyViewState.title
        emptyView.subtitleLabel.text = CardStaticDataSource.emptyViewState.subtitle

        return emptyView
    }
}

private extension CardsViewController {
    func configureNavigationBarMode() {
        toggleNavigationBarMode()
    }

    @objc func toggleMode() {
        toggleTableViewMode()
        toggleNavigationBarMode()
    }

    func editModeSwipeOn() {
        tableView.setEditing(true, animated: true)
        toggleNavigationBarMode()
    }

    func editModeSwipeOff() {
        tableView.setEditing(false, animated: true)
        toggleNavigationBarMode()
    }

    func toggleTableViewMode() {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }

    func toggleNavigationBarMode() {
        navigationItem.leftBarButtonItem = tableView.isEditing ? UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toggleMode)) : UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(toggleMode))

        navigationItem.rightBarButtonItem = tableView.isEditing ? nil : UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddNoteButton))
    }
}

private extension CardsViewController {
    func configureAlertTextField(alertTitle: String, alertMessage: String, indexPath: IndexPath? = nil) {
        var inputTextField: UITextField?

        var object: Card? {
            if let indexPath = indexPath, let object = dataProvider.fetchedResults?.object(at: indexPath) as? Card {
                return object
            }

            return nil
        }

        let actionSheetController: UIAlertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)

        let cancelAction: UIAlertAction = UIAlertAction(title: "Отмена", style: .cancel)
        actionSheetController.addAction(cancelAction)

        let nextAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { [weak self] (_) -> Void in
            if let title = inputTextField?.text, !title.isEmpty {
                if let indexPath = indexPath {
                    self?.dataProvider.updateObject(indexPath: indexPath, with: title)
                } else {
                    self?.dataProvider.createObject(with: title)
                }
            }
        }
        actionSheetController.addAction(nextAction)

        actionSheetController.addTextField { textField -> Void in
            textField.autocapitalizationType = .sentences
            textField.autocorrectionType = .no
            textField.text = object?.recordTitle

            inputTextField = textField
        }

        present(actionSheetController, animated: true, completion: nil)
    }
}

extension CardsViewController: NSFetchedResultsControllerDelegate { // MARK: NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView?.insertSections([sectionIndex], with: .automatic)
        case .delete:
            tableView?.deleteSections([sectionIndex], with: .automatic)
        default:
            return
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let indexPath = newIndexPath else { break }

            tableView?.insertRows(at: [indexPath], with: .automatic)
        case .update:
            guard let indexPath = indexPath else { break }
            guard let cell = tableView?.cellForRow(at: indexPath) else { break }
            guard let object = dataProvider.fetchedResults?.object(at: indexPath) else { break }
            guard let cellModel = OneLabelCellModel.from(managedObject: object) else { break }

            tableManager.customizeCell(cell, cellModel: cellModel, forTableView: tableView)
        case .move:
            print("moved!")
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: .automatic)
            }

            if let newIndexPath = newIndexPath {
                tableView?.insertRows(at: [newIndexPath], with: .automatic)
            }
        case .delete:
            guard let indexPath = indexPath else { break }

            tableView?.deleteRows(at: [indexPath], with: .automatic)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}
