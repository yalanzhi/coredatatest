//
//  EmptyTutorialTableView.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 11.09.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit

final class EmptySubtitleCenterView: UIView {
    var titleLabel: UILabel = UILabel()
    var subtitleLabel: UILabel = UILabel()

    override init (frame: CGRect = .zero) {
        super.init(frame: frame)
        setupLayout()
    }

    func setupLayout() {

        titleLabel.font = UIFont.preferredFont(forTextStyle: .body)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor(named: "SecondaryLabelColor")
        titleLabel.numberOfLines = 0

        subtitleLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        subtitleLabel.textAlignment = .center
        subtitleLabel.textColor = UIColor(named: "SecondaryLabelColor")
        subtitleLabel.numberOfLines = 0

        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 56),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -56),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 54)
            ])
    }

    required init(coder aDecoder: NSCoder) { fatalError("This class does not support NSCoding") }
}
