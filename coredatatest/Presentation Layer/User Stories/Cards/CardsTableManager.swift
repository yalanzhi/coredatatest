//
//  CardsTableManager.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 11.09.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import CoreData

final class CardsTableManager: NSObject {
    var fetchedResults: NSFetchedResultsController<NSManagedObject>? = nil
    
    var didDeleteCell: ((IndexPath) -> Void)?
    var didTapEditNoteButton: ((IndexPath) -> Void)?

    var editModeSwipeOn: (() -> Void)?
    var editModeSwipeOff: (() -> Void)?
    
    func configureTableView(_ tableView: UITableView) {
        tableView.estimatedRowHeight = 54.5
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedSectionFooterHeight = 0.0
        tableView.estimatedSectionHeaderHeight = 0.0
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(cellsClasses: [CardOneLabelCell.self])
    }
}

extension CardsTableManager: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResults?.sections else { return 0 }
        
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResults?.sections else { return 0 }

        return sections[section].numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let object = fetchedResults?.object(at: indexPath) else { return UITableViewCell() }
        guard let model = OneLabelCellModel.from(managedObject: object) else { return UITableViewCell() }

        var cell = UITableViewCell()

        cell = tableView.dequeueReusableCell(withIdentifier: type(of: model).reuseIdentifier, for: indexPath)
        customizeCell(cell, cellModel: model, forTableView: tableView)

        return cell
    }
}

extension CardsTableManager {
    func customizeCell(_ cell: UITableViewCell, cellModel: Reusable, forTableView tableView: UITableView) {
        if let cellModel = cellModel as? OneLabelCellModel, let cell = cell as? CardOneLabelCell {
            cell.configure(with: cellModel)
        }
    }
}

extension CardsTableManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        editModeSwipeOn?()
    }

    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        editModeSwipeOff?()
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { [weak self] (action, view, handler) in
            self?.didDeleteCell?(indexPath)

            handler(true)
        }

        let renameAction = UIContextualAction(style: .normal, title: "Изменить") { [weak self] (action, view, handler) in
            self?.didTapEditNoteButton?(indexPath)

            handler(true)
        }

        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, renameAction])
        configuration.performsFirstActionWithFullSwipe = true

        return configuration
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            didDeleteCell?(indexPath)
        }
    }
}
