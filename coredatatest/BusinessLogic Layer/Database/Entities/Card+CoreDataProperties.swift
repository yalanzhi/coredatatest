//
//  Card+CoreDataProperties.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 28/09/2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//
//

import Foundation
import CoreData


extension Card {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Card> {
        return NSFetchRequest<Card>(entityName: "Card")
    }

    @NSManaged public var recordCreationDate: NSDate?
    @NSManaged public var recordID: UUID?
    @NSManaged public var recordIsDeleted: Bool
    @NSManaged public var recordModificationDate: NSDate?
    @NSManaged public var recordOrderPosition: Int64
    @NSManaged public var recordTitle: String?

}
