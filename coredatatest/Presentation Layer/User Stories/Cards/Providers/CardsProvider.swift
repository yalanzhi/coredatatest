//
//  CardsProvider.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 11.09.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import CoreData

enum CardStaticDataSource {
    case new, edit, emptyViewState

    var title: String {
        switch self {
        case .new:
            return "Новая карточка"
        case .edit:
            return "Переименовать карточку"
        case .emptyViewState:
            return "У вас пока нет карточек."
        }
    }

    var subtitle: String {
        switch self {
        case .new:
            return "Введите название для этой карточки."
        case .edit:
            return "Введите новое имя для этой карточки."
        case .emptyViewState:
            return "Чтобы создать карточку, нажмите на иконку плюса в правом верхнем углу."
        }
    }
}

protocol CardsProtocol {
    var fetchedResults: NSFetchedResultsController<NSManagedObject>? { get }
    
    func fetch(_ completion: (() -> Void)?)

    func createObject(with title: String)
    func updateObject(indexPath: IndexPath, with: String)

    func didDeleteObject(indexPath: IndexPath)
}

final class CardsProvider: CardsProtocol {
    private var delegate: NSFetchedResultsControllerDelegate? = nil
    
    convenience init(fetchedResultsController: NSFetchedResultsControllerDelegate) {
        self.init()
        
        delegate = fetchedResultsController
    }
    
    lazy var fetchedResults: NSFetchedResultsController<NSManagedObject>? = {
        let predicate: NSPredicate? = NSPredicate(format: "recordIsDeleted == false")
        let sortDescriptors: [NSSortDescriptor]? = [NSSortDescriptor(key: "recordOrderPosition", ascending: true), NSSortDescriptor(key: "recordModificationDate", ascending: true)]

        return DatabaseStack.shared.constructFetchedRequestController(for: Card.self, predicate: predicate, sortDescriptors: sortDescriptors, delegate: delegate)
    }()
}

extension CardsProvider {
    func fetch(_ completion: (() -> Void)? = nil) {
        DatabaseStack.shared.fetch(fetchedResultsController: fetchedResults) {
            completion?()
        }
    }
}

extension CardsProvider {
    func createObject(with title: String) {
        Card.create(with: title)

        DatabaseStack.shared.saveContext()
    }

    func updateObject(indexPath: IndexPath, with title: String) {
        guard let object = fetchedResults?.object(at: indexPath) else { return }

        let dictionary: [String: Any] = ["recordTitle": title, "recordModificationDate": Date() as NSDate]

        object.setValuesForKeys(dictionary)
        DatabaseStack.shared.saveContext()
    }
}

extension CardsProvider {
    func didDeleteObject(indexPath: IndexPath) {
        guard let object = fetchedResults?.object(at: indexPath) else { return }

        let dictionary: [String: Any] = ["recordIsDeleted": true, "recordModificationDate": Date() as NSDate]

        object.setValuesForKeys(dictionary)
        DatabaseStack.shared.saveContext()
    }
}

extension Card {
    static func create(with title: String, context: NSManagedObjectContext? = nil) {
        var managedContext: NSManagedObjectContext {
            if let context = context {
                return context
            }

            return DatabaseStack.shared.persistentContainer.viewContext
        }

        let card = Card(context: managedContext)
        card.recordID = UUID()
        card.recordTitle = title
        card.recordIsDeleted = false
        card.recordCreationDate = Date() as NSDate
        card.recordModificationDate = Date() as NSDate
    }
}
