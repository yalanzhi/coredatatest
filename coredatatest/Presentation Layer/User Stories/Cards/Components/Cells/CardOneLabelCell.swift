//
//  OneLabelCell.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 27/09/2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import CoreData

protocol CardCellModelProtocol: Reusable {
    var recordTitle: String? { get }
}

struct OneLabelCellModel: CardCellModelProtocol {
    static var reuseIdentifier: String = CardOneLabelCell.reuseIdentifier
    let recordTitle: String?
}

extension OneLabelCellModel {
    static func from(managedObject: NSManagedObject) -> OneLabelCellModel? {
        var model: OneLabelCellModel? {
            if let object = managedObject as? Card {
                return OneLabelCellModel(recordTitle: object.recordTitle)
            }
            
            return nil
        }
        
        return model
    }
}

final class CardOneLabelCell: UITableViewCell {
    private let titleLabel = UILabel()
    private let dueDateLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with model: OneLabelCellModel) {
        titleLabel.text = model.recordTitle
    }
}

private extension CardOneLabelCell {
    func setupLayout() {
        accessoryType = .disclosureIndicator
        
        titleLabel.font = UIFont.preferredFont(forTextStyle: .body)
        titleLabel.textColor = UIColor(named: "labelColor")
        titleLabel.numberOfLines = 0
        
        let cellStack = UIStackView(arrangedSubviews: [titleLabel])
        cellStack.axis = .vertical
        cellStack.spacing = 6
        cellStack.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(cellStack)
        
        var cellStackLabelBottomConstraint: NSLayoutConstraint {
            let constraint = cellStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12)
            constraint.priority = UILayoutPriority(rawValue: 999)
            
            return constraint
        }
        
        NSLayoutConstraint.activate([
            cellStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            cellStackLabelBottomConstraint,
            cellStack.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            cellStack.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor)
            ])
    }
}
