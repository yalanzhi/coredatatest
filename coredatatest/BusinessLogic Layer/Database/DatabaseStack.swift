//
//  DatabaseStack.swift
//  ricca
//
//  Created by Andrei Ialanzhi on 09.08.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import Foundation
import CoreData

final class DatabaseStack {
    static var shared: DatabaseStack = DatabaseStack()
    
    lazy var privateContext: NSManagedObjectContext = {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = persistentContainer.persistentStoreCoordinator

        return context
    }()

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "panda")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })

        return container
    }()

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func saveContextBackgroundTask(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension DatabaseStack {
    func constructFetchedRequest<T: NSManagedObject>(for entity: T.Type, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil) -> NSFetchRequest<T> {
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: String(describing: entity.self))
        
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.predicate = predicate
        
        return fetchRequest
    }
    
    func constructFetchedRequestController<T: NSManagedObject>(with fetchRequest: NSFetchRequest<T>, managedObjectContext: NSManagedObjectContext? = nil, delegate: NSFetchedResultsControllerDelegate? = nil) -> NSFetchedResultsController<NSManagedObject>? {
        
        var context: NSManagedObjectContext {
            if let managedObjectContext = managedObjectContext {
                return managedObjectContext
            }
            
            return persistentContainer.viewContext
        }
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = delegate
        
        return fetchedResultsController as? NSFetchedResultsController<NSManagedObject>
    }
    
    func constructFetchedRequestController<T: NSManagedObject>(for entity: T.Type,  predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, managedObjectContext: NSManagedObjectContext? = nil, delegate: NSFetchedResultsControllerDelegate? = nil) -> NSFetchedResultsController<NSManagedObject>? {
        let fetchRequest = constructFetchedRequest(for: entity, predicate: predicate, sortDescriptors: sortDescriptors)
        
        return constructFetchedRequestController(with: fetchRequest, managedObjectContext: managedObjectContext, delegate: delegate)
    }
}

extension DatabaseStack {
    func delete(object: NSManagedObject, managedObjectContext: NSManagedObjectContext? = nil) {
        var context: NSManagedObjectContext? {
            if let managedObjectContext = managedObjectContext {
                return managedObjectContext
            }

            return object.managedObjectContext
        }

        context?.delete(object)
    }
}

extension DatabaseStack {
    func fetch(fetchedResultsController: NSFetchedResultsController<NSManagedObject>?, _ completion: (() -> Void)?) {
        do {
            try fetchedResultsController?.performFetch()
            
            completion?()
        } catch (let error) {
            fatalError("\(#function) \(error.localizedDescription)")
        }
    }
}
