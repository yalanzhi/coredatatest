//
//  TableViewSectionModel.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 28/09/2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import Foundation

struct TableViewSectionModel<CellModel> {
    var title: String?
    var cellModels: [CellModel]

    init(_ cellModels: [CellModel]) {
        self.cellModels = cellModels
    }

    init(title: String, cellModels: [CellModel]) {
        self.title = title
        self.cellModels = cellModels
    }
}
