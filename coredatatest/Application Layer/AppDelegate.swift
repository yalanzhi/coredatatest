//
//  AppDelegate.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 07.09.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.makeKeyAndVisible()

        window?.rootViewController = UINavigationController(rootViewController: CardsViewController(style: .grouped))

        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))

        window?.makeKeyAndVisible()
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        DatabaseStack.shared.saveContext()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        DatabaseStack.shared.saveContext()
    }
}
