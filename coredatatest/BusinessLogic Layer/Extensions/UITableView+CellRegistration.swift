//
//  UITableView+CellRegistration.swift
//  coredatatest
//
//  Created by Andrei Ialanzhi on 27/09/2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import UIKit

protocol Reusable {
    static var reuseIdentifier: String { get }
}

extension UITableViewCell: Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewHeaderFooterView: Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func register(cellsClasses: [UITableViewCell.Type]) {
        for item in cellsClasses {
            self.register(item, forCellReuseIdentifier: item.reuseIdentifier)
        }
    }
    
    func register(sectionHeaderFooterCells: [UITableViewHeaderFooterView.Type]) {
        for item in sectionHeaderFooterCells {
            self.register(item, forHeaderFooterViewReuseIdentifier: String(describing: item.self))
        }
    }
    
    func dequeueReusableCell(forClass: UITableViewCell.Type, indexPath: IndexPath) -> UITableViewCell? {
        return self.dequeueReusableCell(withIdentifier: String(describing: forClass.self), for: indexPath)
    }
    
    func dequeueReusableHeaderFooterView(forClass: UITableViewHeaderFooterView.Type) -> UITableViewHeaderFooterView? {
        return self.dequeueReusableHeaderFooterView(withIdentifier: String(describing: forClass.self))
    }
}
